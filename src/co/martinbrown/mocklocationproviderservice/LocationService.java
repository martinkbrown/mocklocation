package co.martinbrown.mocklocationproviderservice;

import java.text.DecimalFormat;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.net.Uri;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

public class LocationService extends Service {

	static Location loc;
	LocationManager locationManager;
	final String DESTINATION_LATITUDE_KEY = "destLatitude";
	final String DESTINATION_LONGITUDE_KEY = "destLongitude";
	final String TAG = "MockLocation";
	
	public static final double[] WAYPOINTS_LAT = {
		30.4466,30.4466,30.4466,30.4466,30.4466,30.4466,30.4466,30.4466,30.4466,30.4466,
		30.4469,30.4471,30.4473,30.4474,30.4471,30.4469,30.4468,30.4466,30.4464,
		30.4462,30.4461,30.4460,30.4458,30.4456,30.4454,30.4452,
		30.4447,30.4443,30.4441,30.4439,30.4436,30.4434,30.4432,30.4430,30.4424,30.4419,
		30.4413,30.4408,30.4401,30.4397,30.4395,30.4392,30.4390,30.4389,30.4387,30.4384,
		30.4381,30.4378,30.4376,30.4374,30.4372,30.4370,30.4370,30.4370,30.4370,30.4370
    };

    public static final double[] WAYPOINTS_LNG = {
    	-84.3001,-84.3001,-84.3001,-84.3001,-84.3001,-84.3001,-84.3001,-84.3001,-84.3001,-84.3001,
    	-84.3007,-84.3011,-84.3013,-84.3015,-84.3016,
    	-84.3014,-84.3015,-84.3015,-84.3016,-84.3018,-84.3020,-84.3022,-84.3022,-84.3023,-84.3024,-84.3024,
    	-84.3024,-84.3024,-84.3025,-84.3025,-84.3025,-84.3025,-84.3027,-84.3027,-84.3028,-84.3031,-84.3031,
    	-84.3029,-84.3026,-84.3023,-84.3023,-84.3023,-84.3024,-84.3025,-84.3025,-84.3025,-84.3025,-84.3025,
    	-84.3027,-84.3027,-84.3030,-84.3032,-84.3032,-84.3032,-84.3032,-84.3032
    };
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		
		final String providerName = LocationManager.GPS_PROVIDER;
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		
		locationManager.addTestProvider(providerName, true, false, false, false, true, true, true,
		                               Criteria.POWER_LOW, Criteria.ACCURACY_FINE);
		
		loc = new Location(providerName);
		
		double interval = ((60.0f / (float) WAYPOINTS_LAT.length));
		DecimalFormat df = new DecimalFormat("#.##");
		String strInterval = df.format(interval);
		int locationSendInterval = (int) interval * 1000;
		
		Toast.makeText(getApplicationContext(), 
				"Sending " + WAYPOINTS_LAT.length + " mock locations, one every " + (int) interval + " seconds",
				Toast.LENGTH_SHORT).show();
		
		Intent navIntent = new Intent();
		
		navIntent.setAction(Intent.ACTION_VIEW);
		navIntent.setClassName("net.osmand.plus", "net.osmand.plus.activities.search.GeoIntentActivity");

		navIntent.setFlags(0x3800000 | Intent.FLAG_ACTIVITY_NEW_TASK);
		
		double destLatitude = 30.436875;
		double destLongitude = -84.303345;
		
		if(intent.hasExtra(DESTINATION_LATITUDE_KEY)) {
			Log.i(TAG, "Trying to get extra " + DESTINATION_LATITUDE_KEY);
			destLatitude = (double) intent.getFloatExtra(DESTINATION_LATITUDE_KEY, (float) destLatitude);
		}
		
		if(intent.hasExtra(DESTINATION_LONGITUDE_KEY)) {
			Log.i(TAG, "Trying to get extra " + DESTINATION_LONGITUDE_KEY);
			destLongitude = (double) intent.getFloatExtra(DESTINATION_LONGITUDE_KEY, (float) destLongitude);
		}
		
		Log.i(TAG, "latitude = " + destLatitude);
		Log.i(TAG, "longitude = " + destLongitude);
		
		navIntent.setData(Uri.parse("geo:0,0?q=" + destLatitude + "," + destLongitude));
		startActivity(navIntent);
		
		new Thread(new Runnable() {
			
			public void run() {

	    		// get a location fix
	    		loc.setLongitude(WAYPOINTS_LNG[0]);
				loc.setLatitude(WAYPOINTS_LAT[0]);
				loc.setAccuracy(3.0f);
				loc.setTime(System.currentTimeMillis());
				
				locationManager.setTestProviderEnabled(providerName, true);
			    
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			    for(int i = 0; i < WAYPOINTS_LAT.length && i < WAYPOINTS_LNG.length; i++) {
						
			    	loc.setLongitude(WAYPOINTS_LNG[i]);
					loc.setLatitude(WAYPOINTS_LAT[i]);
					loc.setAccuracy(3.0f);
					loc.setTime(System.currentTimeMillis());
			    		
					locationManager.setTestProviderStatus(providerName, LocationProvider.AVAILABLE, null, 
							System.currentTimeMillis());
					locationManager.setTestProviderLocation(providerName, loc);
					
					Log.i(TAG, "next latlong = " + WAYPOINTS_LNG[i] + ", " + WAYPOINTS_LAT[i]);

					//navigate for 60 seconds
					try {
						Thread.sleep((60 / WAYPOINTS_LAT.length) * 1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}	
			    
			    stopSelf();
			}
			
		}).start();
		
		return Service.START_NOT_STICKY;
	}

	@Override
	public IBinder onBind(Intent intent) {
		
		return null;
	}
} 
